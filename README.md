# Homework Assignment #1

Homework Assignment #1 for [The Node.js Master Class](https://pirple.thinkific.com/courses/the-nodejs-master-class).

## Assignment

Create a simple "Hello World" API. Meaning:

1. It should be a RESTful JSON API that listens on a port of your choice. 
2. When someone posts anything to the route /hello, you should return a welcome message, in JSON format. This message can be anything you want. 

## Run

```
git clone https://gitlab.com/glejeune/homework-assignment-1
cd homework-assignment-1
node index.js
```

Then :

```
curl -v -X GET http://localhost:3000/hello
```

or 

```
curl -v -k -X GET https://localhost:3001/hello
```
