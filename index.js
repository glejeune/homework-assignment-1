/*
 * @author: Gregoire Lejeune
 */

// Imports
const http = require('http');
const https = require('https');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const config = require('./config');
const fs = require('fs');

// Router
let handlers = {};

handlers.notFound = (data, callback) => {
  callback(404);
};

handlers.ping = (data, callback) => {
  callback(200);
};

handlers.hello = (data, callback) => {
  callback(200, {"response": "World"});
};

const router = {
  'ping' : handlers.ping,
  'hello': handlers.hello
};

// Main server function
const unifiedServer = (req, res) => {
  // Path
  const parsedUrl = url.parse(req.url, true)
  var path = parsedUrl.pathname.replace(/^\/+|\/+$/g, '');

  // Query string
  const queryStringObject = parsedUrl.query;

  // HTTP method
  const method = req.method.toLowerCase();

  // Headers
  const headers = req.headers;

  // Body decoder
  const decoder = new StringDecoder('utf-8');
  let buffer = '';
  req.on('data', function(data) {
    buffer += decoder.write(data);
  });
  req.on('end', function() {
    buffer += decoder.end();

    // Get handler for request
    const chosenHandler = typeof(router[path]) !== 'undefined' ? router[path] : handlers.notFound;

    // Data object passed to the handler
    const data = {
      'path' : path,
      'queryStringObject' : queryStringObject,
      'method' : method,
      'headers' : headers,
      'payload' : buffer
    };

    // Call handler
    chosenHandler(data, (statusCode, payload) => {
      statusCode = typeof(statusCode) == 'number' ? statusCode : 200;
      payload = typeof(payload) == 'object'? payload : {};

      const payloadString = JSON.stringify(payload);

      res.setHeader('Content-Type', 'application/json');
      res.writeHead(statusCode);
      res.end(payloadString);
      console.log("[%s] - %s /%s - %d", new Date().toISOString(), method.toUpperCase(), path, statusCode);
    });
  });
};

// Create and start HTTP server
http.createServer((req, res) => {
  unifiedServer(req, res);
}).listen(config.httpPort, () => {
  console.log('The HTTP server is running on port '+config.httpPort);
});

// Create and start HTTPS server
const httpsServerOptions = {
  'key': fs.readFileSync('./https/key.pem'),
  'cert': fs.readFileSync('./https/cert.pem')
};
https.createServer(httpsServerOptions, (req, res) => {
  unifiedServer(req, res);
}).listen(config.httpsPort, () => {
  console.log('The HTTPS server is running on port '+config.httpsPort);
});
